package id.ac.ui.cs.advprog.tutorial4.abstractfactory;

import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.DrangleicAcademy;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.KnightAcademy;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.MajesticKnight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.Knight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.MetalClusterKnight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.SyntheticKnight;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;


public class DrangleicAcademyTest {
    KnightAcademy drangleicAcademy;
    Knight majesticKnight;
    Knight metalClusterKnight;
    Knight syntheticKnight;

    @BeforeEach
    public void setUp() {
        // TODO setup me
        drangleicAcademy = new DrangleicAcademy();
        majesticKnight = drangleicAcademy.getKnight("majestic");
        metalClusterKnight = drangleicAcademy.getKnight("metal cluster");
        syntheticKnight = drangleicAcademy.getKnight("synthetic");


    }

    @Test
    public void checkKnightInstances() {
        // TODO create test
        assertNotNull(majesticKnight);
        assertNotNull(metalClusterKnight);
        assertNotNull(syntheticKnight);

    }

    @Test
    public void checkKnightNames() {
        // TODO create test
        majesticKnight.setName("Majestic Knight");
        assertEquals(majesticKnight.getName(), "Majestic Knight");
        metalClusterKnight.setName("Metal Cluster Knight");
        assertEquals(metalClusterKnight.getName(), "Metal Cluster Knight");
        syntheticKnight.setName("Synthetic Knight");
        assertEquals(syntheticKnight.getName(), "Synthetic Knight");


    }

    @Test
    public void checkKnightDescriptions() {
        // TODO create test
        assertNotNull(majesticKnight.getArmor());
        assertNotNull(majesticKnight.getWeapon());
        assertNull(majesticKnight.getSkill());

        assertNotNull(metalClusterKnight.getArmor());
        assertNull(metalClusterKnight.getWeapon());
        assertNotNull(metalClusterKnight.getSkill());

        assertNull(syntheticKnight.getArmor());
        assertNotNull(syntheticKnight.getWeapon());
        assertNotNull(syntheticKnight.getSkill());

    }

}
