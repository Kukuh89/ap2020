package id.ac.ui.cs.advprog.tutorial4.abstractfactory;

import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.LordranAcademy;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.KnightAcademy;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.MajesticKnight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.Knight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.MetalClusterKnight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.SyntheticKnight;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;


public class LordranAcademyTest {
    KnightAcademy lordranAcademy;
    Knight majesticKnight;
    Knight metalClusterKnight;
    Knight syntheticKnight;

    @BeforeEach
    public void setUp() {
        // TODO setup me
        lordranAcademy = new LordranAcademy();
        majesticKnight = lordranAcademy.getKnight("majestic");
        metalClusterKnight = lordranAcademy.getKnight("metal cluster");
        syntheticKnight = lordranAcademy.getKnight("synthetic");

    }

    @Test
    public void checkKnightInstances() {
        // TODO create test
        assertNotNull(majesticKnight);
        assertNotNull(metalClusterKnight);
        assertNotNull(syntheticKnight);

    }

    @Test
    public void checkKnightNames() {
        // TODO create test
        majesticKnight.setName("Majestic Knight");
        assertEquals(majesticKnight.getName(), "Majestic Knight");
        metalClusterKnight.setName("Metal Cluster Knight");
        assertEquals(metalClusterKnight.getName(), "Metal Cluster Knight");
        syntheticKnight.setName("Synthetic Knight");
        assertEquals(syntheticKnight.getName(), "Synthetic Knight");


    }

    @Test
    public void checkKnightDescriptions() {
        // TODO create test
        assertNotNull(majesticKnight.getArmor());
        assertNotNull(majesticKnight.getWeapon());
        assertNull(majesticKnight.getSkill());

        assertNotNull(metalClusterKnight.getArmor());
        assertNull(metalClusterKnight.getWeapon());
        assertNotNull(metalClusterKnight.getSkill());

        assertNull(syntheticKnight.getArmor());
        assertNotNull(syntheticKnight.getWeapon());
        assertNotNull(syntheticKnight.getSkill());
    }
}
