package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithBarrier implements DefenseBehavior {
        //ToDo: Complete me
        public String defend(){
                return "Defended by a Barrier";
        }

        public String getType(){
                return "Defense with Barrier";
        }
}
