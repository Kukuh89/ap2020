package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithArmor implements DefenseBehavior {
        //ToDo: Complete me
        public String defend(){
                return "Defended by an Armor";
        }

        public String getType(){
                return "Defense With Armor";
        }
}
