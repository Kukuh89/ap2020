package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithShield implements DefenseBehavior {
        //ToDo: Complete me
        public String defend(){
                return "Defended by a Shield";
        }
        public String getType(){
                return "Defense With Shield";
        }
}
