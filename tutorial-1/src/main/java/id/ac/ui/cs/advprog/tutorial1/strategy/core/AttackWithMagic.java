package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithMagic implements AttackBehavior {
        //ToDo: Complete me
        public String attack(){
                return "Attacked using Magic";
        }

        public String getType(){
                return "Attack With Magic";
        }
}
