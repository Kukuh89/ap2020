package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import java.util.ArrayList;

public class ChainSpell implements Spell {
    // TODO: Complete Me

    private ArrayList<Spell> chains;

    public ChainSpell(ArrayList<Spell> arrayList) {
        chains = arrayList;        
	}

	@Override
    public String spellName() {
        return "ChainSpell";
    }

    @Override
    public void cast() {
        // TODO Auto-generated method stub
        for (Spell spell : chains) {
            spell.cast();
        }
    }

    @Override
    public void undo() {
        // TODO Auto-generated method stub
        for (int i = chains.size(); i > 0; i--) {
            chains.get(i).undo();
        }
    }
}
