package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import id.ac.ui.cs.advprog.tutorial2.command.core.spirit.HighSpirit;

public class HighSpiritStealthSpell extends HighSpiritSpell {
    // TODO: Complete Me

    public HighSpiritStealthSpell(HighSpirit spirit) {
        super(spirit);
        // TODO Auto-generated constructor stub
    }

    @Override
    public void cast() {
        // TODO Auto-generated method stub
        spirit.stealthStance();
    }

    @Override
    public String spellName() {
        // TODO Auto-generated method stub
        return spirit.getRace() + ":Stealth";
    }
}
