package id.ac.ui.cs.advprog.tutorial3.composite.core;

import java.util.ArrayList;
import java.util.List;

public class PremiumMember implements Member {

        private String name, role;
        private List<Member> memberList;

        public PremiumMember(String name, String role){
                this.name = name;
                this.role = role;
                memberList = new ArrayList<>();
        }

        @Override
        public String getName() {
                // TODO Auto-generated method stub
                return name;
        }

        @Override
        public String getRole() {
                // TODO Auto-generated method stub
                return role;
        }

        @Override
        public void addChildMember(Member member) {
                // TODO Auto-generated method stub
                if(memberList.size() < 3 || role.equals("Master")){
                        memberList.add(member);
                }
        }

        @Override
        public void removeChildMember(Member member) {
                // TODO Auto-generated method stub
                memberList.remove(member);

        }

        @Override
        public List<Member> getChildMembers() {
                // TODO Auto-generated method stub
                return memberList;
        }
        //TODO: Complete me
}
