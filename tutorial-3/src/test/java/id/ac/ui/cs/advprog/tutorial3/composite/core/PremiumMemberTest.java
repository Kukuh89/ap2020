package id.ac.ui.cs.advprog.tutorial3.composite.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PremiumMemberTest {
    private Member member;

    @BeforeEach
    public void setUp() {
        member = new PremiumMember("Wati", "Gold Merchant");
    }

    @Test
    public void testMethodGetName()
    {
        //TODO: Complete me

        assertEquals("Wati", member.getName());
    }

    @Test
    public void testMethodGetRole() {
        //TODO: Complete me

        assertEquals("Gold Merchant", member.getRole());
    }

    @Test
    public void testMethodAddChildMember() {
        //TODO: Complete me

        Member member = new PremiumMember("Knight", "Fighter");
        member.addChildMember(member);
        assertEquals(1, member.getChildMembers().size());
    }

    @Test
    public void testMethodRemoveChildMember() {
        //TODO: Complete me
        Member member = new PremiumMember("Knight", "Fighter");
        member.addChildMember(member);
        member.removeChildMember(member);
        assertEquals(0, member.getChildMembers().size());
    }

    @Test
    public void testNonGuildMasterCanNotAddChildMembersMoreThanThree() {
        //TODO: Complete me
        member.addChildMember(new PremiumMember("Martis", "Fighter"));
        member.addChildMember(new PremiumMember("Layla", "Marksman"));
        member.addChildMember(new PremiumMember("Nana", "Support"));
        member.addChildMember(new PremiumMember("Jonh", "Tank"));
        assertEquals(3, member.getChildMembers().size());

    }

    @Test
    public void testGuildMasterCanAddChildMembersMoreThanThree() {
        //TODO: Complete me
        Member master = new PremiumMember("Haris", "Master");
        master.addChildMember(new PremiumMember("Josua", "Second"));
        master.addChildMember(new PremiumMember("Karman", "Second"));
        master.addChildMember(new PremiumMember("Edgy", "Second"));
        master.addChildMember(new PremiumMember("Sussy", "Second"));
        assertEquals(4, master.getChildMembers().size());

    }
}
